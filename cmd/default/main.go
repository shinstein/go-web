package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

/*
使用http包启动web服务
*/
func main() {
	http.HandleFunc("/hello/", hello)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		panic(err)
	}
}

/*
curl --request POST --url 'http://localhost:9090/hello/ZhangSan/22?name=LiSi&age=18' --header 'content-type: application/json' --data '{"name": "LiSi","password": "123"}'

http://localhost:9090/hello/ZhangSan/22?name=LiSi&age=18

body:

	{
		"code": 200,
		"message": "success"
	}
*/
func hello(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Header:", r.Header)         // map[Accept:[*/*] Accept-Encoding:[gzip, deflate, br] Cache-Control:[no-cache] Connection:[keep-alive] Content-Length:[48] Content-Type:[application/json] User-Agent:[PostmanRuntime-ApipostRuntime/1.1.0]]
	fmt.Println("Method:", r.Method)         // POST
	fmt.Println("Host:", r.Host)             // localhost:9090
	fmt.Println("RequestURI:", r.RequestURI) // /hello/ZhangSan/22?name=LiSi&age=18

	bodyBytes, err := io.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	/*
		{
		    "name": "LiSi",
		    "password": "123"
		}
	*/
	fmt.Println("body:", string(bodyBytes))

	name := r.URL.Query().Get("name")
	age := r.URL.Query().Get("age")

	fmt.Println("age =", age, ", name =", name) // age = 18 , name = LiSi

	// 获取uri里的参数
	path := r.URL.Path
	fmt.Println("path =", path)
	split := strings.Split(path, "/")
	fmt.Println("split =", split)                        // [ hello ZhangSan 22] // 切片第一个元素是空
	fmt.Println("name =", split[2], ", age =", split[3]) // name = ZhangSan , age = 22

	w.WriteHeader(http.StatusOK)

	m := map[string]any{
		"code":    200,
		"message": "success",
	}

	dataBytes, err := json.Marshal(m)
	if err != nil {
		panic(err)
	}

	// 设置响应头
	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(dataBytes)
	if err != nil {
		http.Error(w, "Error writing response", http.StatusInternalServerError)
		return
	}
}
