package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func main() {
	router := mux.NewRouter()

	// http://localhost:9091/hello/ZhangSan/22
	router.HandleFunc("/hello/{name}/{age}", hello)

	http.Handle("/", router)

	err := http.ListenAndServe(":9091", nil)
	if err != nil {
		panic(err)
	}
}

/*
curl --request POST --url http://localhost:9091/hello/ZhangSan/22
*/
func hello(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	name := vars["name"]
	age := vars["age"]

	fmt.Println("name =", name, ", age =", age) // name = ZhangSan , age = 22

	w.WriteHeader(http.StatusOK)

	// 设置响应头
	w.Header().Set("Content-Type", "application/json")

	dataBytes, _ := json.Marshal(map[string]any{
		"code":    200,
		"message": "success",
	})
	_, err := w.Write(dataBytes)

	if err != nil {
		panic(err)
	}
}
