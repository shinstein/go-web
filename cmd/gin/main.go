package main

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"strings"
	"time"
)

/*
要将 request body中的数据绑定到 json 数据中，结构体的字段名必须是导出的(首字母大写)
*/
type user struct {
	Name     string `json:"name" uri:"name" form:"name" `
	Password string `json:"password" uri:"password" form:"password"`
}

func Global1() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		request := ctx.Request
		fmt.Println("Global1: start")
		fmt.Println("ctx.HandlerName():", ctx.HandlerName()) // main.addUser
		fmt.Println("ctx.Params:", ctx.Params)               // []
		fmt.Println("request.Host:", request.Host)           // localhost:9999
		fmt.Println("request.Method:", request.Method)       //POST

		// ctx.GetRawData() 读取请求体，读取完成后导致请求体中没有数据了，谨慎使用
		//bytes, _ := ctx.GetRawData()

		bodyBytes, _ := io.ReadAll(request.Body)

		fmt.Println("request.Body:", string(bodyBytes)) // {"name":"LiSi","password":"123"}
		fmt.Println("request.Header:", request.Header)

		// 重新设置请求体，以便后续中间件和处理函数能够读取
		request.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
	}
}

func Global2() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		now := time.Now()
		fmt.Println("Global2:start ...")

		// 执行实际方法
		ctx.Next()

		duration := time.Since(now)
		fmt.Println("Global2:finish ..., Spend time =", duration)
	}
}

func MiddleWare() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fmt.Println("MiddleWare ...")
	}
}

func main() {
	engine := gin.Default()

	//engine.Use(Global2()) // Global2 先注册，先执行
	//engine.Use(Global1()) // Global1 后注册，后执行

	engine.Use(Global2(), Global1())

	/*
		curl --request GET --url http://localhost:9999/hello

		resp:
		{
			"message": "Hello Gin"
		}
	*/
	engine.GET("/hello", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Hello Gin",
		})
	})

	engine.GET("/", homePage)

	/*
		http://localhost:9999/LiSi/haha

		resp:
		{
			"action": "haha",
			"user": "LiSi"
		}
	*/
	engine.GET("/:user/*action", param)

	/*
		http://localhost:9999/user?user=LiSi&age=18

		{
		    "age": "18",
		    "user": "LiSi"
		}
	*/
	engine.GET("/user", query)

	// 模拟新增用户
	/*
		该请求结构体必须要有 json 的tag
		curl --request POST --url http://localhost:9999/user --header 'content-type: application/json' --data '{"name": "LiSi","password": "123"}'

		{
			"name": "LiSi",
			"password": "123"
		}
	*/
	engine.POST("/user", addUser)

	/*
		该请求结构体必须要有 uri 的tag
		curl --request POST --url http://localhost:9999/user/LiSi/123
		{
			"name": "LiSi",
			"password": "123"
		}
	*/
	engine.POST("/user/:name/:password", getUser)

	/*
		该请求结构体必须要有 form 的tag
		curl --request POST --url 'http://localhost:9999/user/query?name=LiSi&password=123'

		{
			"name": "LiSi",
			"password": "123"
		}
	*/
	engine.POST("/user/query", queryUser)

	// 请求重定向
	engine.GET("/redirect", redirect)

	//测试针对指定请求的中间件
	engine.GET("/aa/bb", MiddleWare(), func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "success",
		})
	})

	// 指定端口为 9999, 默认端口为 8080
	err := engine.Run(":9999")
	if err != nil {
		return
	}
}

func redirect(ctx *gin.Context) {
	ctx.Redirect(http.StatusMovedPermanently, "https://www.baidu.com")
}

func queryUser(ctx *gin.Context) {
	var u user

	if err := ctx.ShouldBindQuery(&u); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"err": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, u)
}

func getUser(ctx *gin.Context) {
	var u user
	if err := ctx.ShouldBindUri(&u); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"err": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, u)
}

func addUser(ctx *gin.Context) {
	fmt.Println("addUser ...")
	var u user

	// 将request body 中的json 数据绑定到 user 结构体
	if err := ctx.ShouldBindJSON(&u); err != nil {
		fmt.Println(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"err": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, u)
}

func homePage(ctx *gin.Context) {
	ctx.String(http.StatusOK, "Welcome to Gin")
}

func param(ctx *gin.Context) {
	user := ctx.Param("user")
	action := ctx.Param("action") // action = /haha

	ctx.JSON(http.StatusOK, gin.H{
		"user":   user,
		"action": strings.Trim(action, "/"),
	})
}

func query(ctx *gin.Context) {
	user := ctx.Query("user")
	age := ctx.Query("age")

	ctx.JSON(http.StatusOK, gin.H{
		"user": user,
		"age":  age,
	})
}
